//
//  main.m
//  Warehouse
//
//  Created by Ilya Rezyapkin on 21.03.14.
//  Copyright (c) 2014 Ilya Rezyapkin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AJAppDelegate class]));
    }
}
