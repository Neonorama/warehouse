//
//  AJAppDelegate.h
//  Warehouse
//
//  Created by Ilya Rezyapkin on 21.03.14.
//  Copyright (c) 2014 Ilya Rezyapkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
